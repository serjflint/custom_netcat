#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import socket
import errno
import argparse
import sys
import trio


def parse_args():
    parser: argparse.ArgumentParser = argparse.ArgumentParser()
    parser.add_argument(
        "-a",
        "--host-address",
        default="127.0.0.1",
        help="Host address in 0.0.0.0 format",
    )
    parser.add_argument(
        "-p",
        "--port",
        type=int,
        default=37038,
        help="Host address port",
    )
    parser.add_argument(
        "-b",
        "--binary",
        action='store_true',
        default=False,
        help="Is data a hex string or ascii",
    )
    parser.add_argument(
        "-s",
        "--synchronous",
        action='store_true',
        default=False,
        help="To use synchronous client instead of trio",
    )
    parser.add_argument(
        "data",
        nargs="*",
        help="Data to send",
    )
    args, leftovers = parser.parse_known_args()
    return args


def serve(host: str, port: int = 37038, binary: bool = False, send_data: list = ()):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, port))

        print("Data to send:")

        while True:
            if send_data:
                lines = send_data
                send_data = None
            else:
                text = input()
                lines = text.split()

            for line in lines:
                if binary:
                    line = bytes.fromhex(line)
                else:
                    line = line.encode()

                s.sendall(line)

            received = s.recv(1024)

            if binary:
                received = received.hex()
            else:
                received = received.decode()
            print(received)


async def sender(client_stream, binary: bool = False, send_data: list = ()):
    print("sender: started!")
    while True:
        if send_data:
            lines = send_data
            send_data = None
        else:
            text = input()
            lines = text.split()

        for line in lines:
            print(f"sender: sending {line}")

            if binary:
                packet = bytes.fromhex(line)
            else:
                packet = line.encode()

            await client_stream.send_all(packet)
            await trio.sleep(1)


async def receiver(client_stream, binary: bool = False):
    print("receiver: started!")
    async for data in client_stream:
        if binary:
            received = data.hex()
        else:
            received = data.decode()
        print(f"receiver: got data {received}")
    print("receiver: connection closed")
    sys.exit()


async def parent(host: str = "127.0.0.1", port: int = 37038, binary: bool = False, send_data: list = ()):
    print(f"parent: connecting to {host}:{port}")
    client_stream = await trio.open_tcp_stream(host, port)
    async with client_stream:
        async with trio.open_nursery() as nursery:
            print("parent: spawning sender...")
            nursery.start_soon(sender, client_stream, binary, send_data)

            print("parent: spawning receiver...")
            nursery.start_soon(receiver, client_stream, binary)


if __name__ == "__main__":
    args = parse_args()

    host = args.host_address
    port = args.port

    try:
        if args.synchronous:
            serve(host, port, args.binary, args.data)
        else:
            trio.run(parent, host, port, args.binary, args.data)

    except (KeyboardInterrupt, SystemExit) as err:
        pass
    except socket.error as err:
        if err.errno == errno.ECONNREFUSED:
            print(f"Port is not open {host}:{port}")
        elif err.errno == errno.ECONNRESET:
            print(f"Connection reset by peer {host}:{port}")
        else:
            print(f"Other socket error: {err}")
